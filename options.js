function saveOptions(e) {
  browser.storage.sync.set({
    gnusocialHost: document.querySelector("#gnusocialHost").value,
    mastodonHost: document.querySelector("#mastodonHost").value,
    hubzillaHost: document.querySelector("#hubzillaHost").value,
    diasporaHost: document.querySelector("#diasporaHost").value,
    friendicaHost: document.querySelector("#friendicaHost").value,
    socialhomeHost: document.querySelector("#socialhomeHost").value,
    prismoHost: document.querySelector("#prismoHost").value,
    shortcut: document.querySelector("#shortcut").value
  });
  e.preventDefault();
}

function restoreOptions() {

  var gettingGnusocial = browser.storage.sync.get('gnusocialHost');
  gettingGnusocial.then((res) => {
    document.querySelector("#gnusocialHost").value = res.gnusocialHost;
  });

  var gettingMastodon = browser.storage.sync.get('mastodonHost');
  gettingMastodon.then((res) => {
    document.querySelector("#mastodonHost").value = res.mastodonHost;
  });

  var gettingHubzilla = browser.storage.sync.get('hubzillaHost');
  gettingHubzilla.then((res) => {
    document.querySelector("#hubzillaHost").value = res.hubzillaHost;
  });

  var gettingDiaspora = browser.storage.sync.get('diasporaHost');
  gettingDiaspora.then((res) => {
    document.querySelector("#diasporaHost").value = res.diasporaHost;
  });

  var gettingFriendica = browser.storage.sync.get('friendicaHost');
  gettingFriendica.then((res) => {
    document.querySelector("#friendicaHost").value = res.friendicaHost;
  });

  var gettingSocialhome = browser.storage.sync.get('socialhomeHost');
  gettingSocialhome.then((res) => {
    document.querySelector("#socialhomeHost").value = res.socialhomeHost;
  });

  var gettingPrismo = browser.storage.sync.get('prismoHost');
  gettingPrismo.then((res) => {
    document.querySelector("#prismoHost").value = res.prismoHost;
  });

  var gettingShortcut = browser.storage.sync.get('shortcut');
  gettingShortcut.then((res) => {
    document.querySelector("#shortcut").value = res.shortcut;
  });
}

/**
 * Keyboard shortcut
 */

const commandShorcut = '_execute_browser_action';

/**
 * Update the UI: set the value of the shortcut textbox.
 */
async function updateUI() {
  let commands = await browser.commands.getAll();
  for (command of commands) {
    if (command.name === commandShorcut) {
      document.querySelector('#shortcut').value = command.shortcut;
    }
  }
}

/**
 * Update the shortcut based on the value in the textbox.
 */
async function updateShortcut() {
  await browser.commands.update({
    name: commandShorcut,
    shortcut: document.querySelector('#shortcut').value
  });
}

/**
 * Reset the shortcut and update the textbox.
 */
async function resetShortcut() {
  await browser.commands.reset(commandShorcut);
  updateUI();
}

/**
 * Update the UI when the page loads.
 */

document.addEventListener('DOMContentLoaded', restoreOptions, updateUI);
document.querySelector("form").addEventListener("submit", saveOptions);
document.querySelector('#update').addEventListener('click', updateShortcut)
document.querySelector('#reset').addEventListener('click', resetShortcut)
