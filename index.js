/*
Original by yuvaraj
Copyright (c) 2016
Forked by Mickie
Copyleft (ɔ) 2019
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


;
(function () {
  'use strict';

  // Send Message To Background Script

  function sendMessage(tab) {
    const { id } = tab;
    browser.runtime.sendMessage({
      type: 'sharer-tab-id',
      data: { id }
    });
  }

  /*
   * Assign URLs To Respective Element
   */
  function urlAssigner(btn = {}) {
    const { id, href } = btn;
    const a = document.getElementById(id);
    a.href = href;
    a.target = '_blank';
    a.addEventListener('click', event => {
      event.preventDefault();
      const { href } = event.target;
      chrome.tabs.create({ url: href }, function (tab) {
	// console.log(tab, tab.id);
	sendMessage(tab)
      })
      window.setTimeout(() => window.close(), 10)
    });
  }

  /*
   * Make Social Button Objects
   */

  function mkBtns(tabUrl = '', tabTitle = '') {

    // Set hosts

    var gnusocialItem = browser.storage.sync.get('gnusocialHost');
    gnusocialItem.then((res) => {
      document.querySelector("#url-gnusocial").href = res.gnusocialHost + `?action=newnotice&status_textarea=${tabTitle} ${tabUrl}`;
    });

    var mastodonItem = browser.storage.sync.get('mastodonHost');
    mastodonItem.then((res) => {
      document.querySelector("#url-mastodon").href = res.mastodonHost + `/share?text=${tabTitle}&url=${tabUrl}`;
    });

    var hubzillaItem = browser.storage.sync.get('hubzillaHost');
    hubzillaItem.then((res) => {
      document.querySelector("#url-hubzilla").href = res.hubzillaHost + `/rpost?body=${tabTitle} &url=${tabUrl}`;
    });

    var diasporaItem = browser.storage.sync.get('diasporaHost');
    diasporaItem.then((res) => {
      document.querySelector("#url-diaspora").href = res.diasporaHost + `/bookmarklet?url=${tabUrl}&title=${tabTitle}&jump-doclose`;
    });

    var friendicaItem = browser.storage.sync.get('friendicaHost');
    friendicaItem.then((res) => {
      document.querySelector("#url-friendica").href = res.friendicaHost + `/bookmarklet?url=${tabUrl}&title=${tabTitle}&jump-doclose`;
    });

    var pumpioItem = `https://www.autistici.org/granada/url.php?url=${tabUrl}`; // Post to Granada webapp

    var socialhomeItem = browser.storage.sync.get('socialhomeHost');
    socialhomeItem.then((res) => {
      document.querySelector("#url-socialhome").href = res.socialhomeHost + `/bookmarklet?url=${tabUrl}&title=${tabTitle}&jump-doclose`;
    });

    var prismoItem = browser.storage.sync.get('prismoHost');
    prismoItem.then((res) => {
      document.querySelector("#url-prismo").href = res.prismoHost + `/posts/new?url=${tabUrl}&title=${tabTitle}`;
    });

    // Set social Buttons

    const socialBtns = [];

    const gnusocial = {};
    gnusocial.href = gnusocialItem;
    gnusocial.id = 'url-gnusocial';
    socialBtns.push(gnusocial);

    const mastodon = {};
    mastodon.href =  mastodonItem;
    mastodon.id = 'url-mastodon';
    socialBtns.push(mastodon);

    const hubzilla = {};
    hubzilla.href = hubzillaItem;
    hubzilla.id = 'url-hubzilla';
    socialBtns.push(hubzilla);

    const diaspora = {};
    diaspora.href = diasporaItem;
    diaspora.id = 'url-diaspora';
    socialBtns.push(diaspora);

    const friendica = {};
    friendica.href = friendicaItem;
    friendica.id = 'url-friendica';
    socialBtns.push(friendica);

    const pumpio = {};
    pumpio.href = pumpioItem;
    pumpio.id = 'url-pumpio';
    socialBtns.push(pumpio);

    const socialhome = {};
    socialhome.href = socialhomeItem;
    socialhome.id = 'url-socialhome';
    socialBtns.push(socialhome);

    const prismo = {};
    prismo.href = prismoItem;
    prismo.id = 'url-prismo';
    socialBtns.push(prismo);

    socialBtns.forEach(urlAssigner);
  }

  ;
  (function getCurrentTabUrl() {
    const queryInfo = {
      active: true,
      currentWindow: true
    };

    chrome.tabs.query(queryInfo, (tabs = []) => {
      if (tabs.length === 0) {
	return;
      }

      const tab = (tabs[0] || {});
      const tabUrL = tab.url;
      const tabTitle = tab.title;
      mkBtns(tabUrL, tabTitle);
    });

    // End
  })();

})();

/*
 * Make Shortcut
 */

let gettingAllCommands = browser.commands.getAll();
gettingAllCommands.then((commands) => {
  for (let command of commands) {
    console.log(command);
  }
});

browser.commands.onCommand.addListener(function(command) {
  if (command == "_execute_browser_action") {
    // console.log("execute the feature!");
  }
});
