# ![](/img/icon64.png) Share freedom - Firefox addon
Icon by Alexandr Martinov
![](/img/captura.png)

Share the current tab on the fediverse.

## Description
Simplest add-on for the fediverse. Share web pages right from the toolbar button.

Supported services/protocols:
- Gnusocial
- Mastodon
- Hubzilla
- Diaspora
- Friendica
- Pump.io
- Socialhome
- Prismo

This add-on is a fork of [URL-sharer](https://github.com/shivarajnaidu/URL-sharer) (for centralized/privative social
networks) by shivarajnaidu.

## Features
- Enable options page: Firefox menu -> Add-ons -> __Share-freedom preferences__
- Run the app using keyboard shortcut, by default __Ctrl+Shift+B__

## Note
- Pump.io → redirects to [Granada](https://www.autistici.org/granada/index.php), a complete sharing content webapp. Website in Spanish.

## Build
Run on project root directory

```bash
npm install
gulp
```

## Resources
issues: https://gitlab.com/mickie1/share-freedom-addon/issues

## Support - Invite an ice cream
You can collaborate with some __criptocurrencies__ for the project :)

__faircoins:__ fVXruVLErxawnKAchcfbMQXya8YYYPvaZN

__Bitcoins:__ 15Y9cBeGhGmVGZxFQvQCsQhcCvxgzvKmcR

![](/img/helado.png)
[CC-BY 4.0 by Vincent Le Moign](https://commons.wikimedia.org/wiki/File:552-soft-ice-cream-2.svg)

## License

This Program is Licensed under GPL v3

Copyleft (ɔ) 2019 - Forked by miguel (mickie)

See LICENSE file for more information.
